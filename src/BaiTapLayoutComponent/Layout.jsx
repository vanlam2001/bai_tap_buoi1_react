import React, { Component } from 'react'
import Banner from './Banner'
import Header from './Header'
import Item from './Item'
import Footer from './Footer'
export default class Layout extends Component {
  render() {
    return (
      <div>
        <Header></Header>
        <Banner></Banner>
        <Item></Item>
        <Footer></Footer>
      </div>
    )
  }
}
